# AutoTyper
## Software

The firmware in this repo is all writen in the Arduino flavor of C++. AutoTyper.ino contains the complete firmware. This Firmware relies on the Arduino Keyboard and SD libraries as well as [ArduinoJSON by Benoît Blanchon](https://arduinojson.org/v6/). The firmare is setup to output Debug messages to the serial monitor at 9600 baud. With out a properly formatted SD card inserted the firware will write `SD Card INIT Failed!` to the serial console and do nothing else. Without at least one valid JSON config file on the SD card the firmware will initialize the SD card find no configuration and do nothing. Valid config filenames are `auto.json` for a file to be run at power on with no input; layer files are named `1.json`, `2.json` ... `n.json`. If there is an `auto.json` file on the SD card when the device is plugged in it will be run and then the device will wait for input. If there are any valid layer files you can cycle through them with the select button `1.json` is selected by default and the number is incremented each time the select button is pressed and reset to one whenever the next file is not found allowing you to choose any layer file in the sequence 1 to n. The resultant layer file selection will be displayed by blinking the LED. The selected layer file will be run when the type button is pressed. The LED will be on whenever the device is typing something.

## Configuration

Examples of valid configs can be found in the `example_json_configs` folder.

### Top Level Config
Valid configuration files contain a single JSON object with a single level zero key `steps` which is and array of n JSON step objects.

``` json
{"steps": [/* step objects go here */]}
```

### Step Objects

So far there are three types of step objects: type, press, and delay. All step objects have a key `action` that contains a string denoting the type of command it is.

#### type step objects
Type step objects has a key `action` containing the string `type` and a key `string` to type containing a string to type.

Example: to type a tab followed by the word bob and a newline you would append the step object:
``` json
{"action": "type", "string": "\tbob\n"}
```

#### press step objects
Press step objects has a key `action` containing the string `press` and a key `key` to type containing a list of integer key codes to press. A list of keycodes can be found at: https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/ and for alpha-numeric-symbol keys ascii codes can be used.

Example: to press the key combination SUPER+d you would append the step object:
``` json
{"action": "press", "keys": [135, 100]}
```

Example: to press the key combination SUPER+ENTER you would append the step object:
``` json
{"action": "press", "keys": [135, 176]}
```

#### delay step objects
Delay step objects has a key `action` containing the string `delay` and a key `ms` to type containing an integer number of milliseconds to pause execution for.

Example: to wait for 30 seconds before performing the next action you would append the step object:
``` json
{"action": "delay", "ms": 30000}
```

## Hardware

### Circuit Diagram

![](AutoTyper_Circuit_Diagram.png)

### Parts

* Micro Controller: Pro Micro by Tenstar Robot (ATMEL MEGA32U4-MU) - https://www.amazon.com/gp/product/B01HCXMBOU/
* SD Card Module: Adafruit MicroSD Card Breakout Board+ (ADA254) - https://www.amazon.com/gp/product/B00NAY2NAI/
* R1 220 Ohm Resistor
* 1 Blue LED
* 2x Momentary Switches
