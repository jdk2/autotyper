#include <SD.h>
#include <Keyboard.h>
#include <ArduinoJson.h>

// https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/

int BlueIndicatorLEDPin = 5;
int TypeButtonPin = 6;
int SelectButtonPin = 7;
int AutoRunSwitchPin = 8;
int SDCardCSPin = 10;
int selectedFile = 1;
int val = 0;


void led_on() {
  digitalWrite(BlueIndicatorLEDPin, HIGH);
}


void led_off() {
  digitalWrite(BlueIndicatorLEDPin, LOW);
}


void type_string(String out) {
  for (int i = 0; i < out.length(); i++) {
    Keyboard.write(out.charAt(i));
    delay(10);
  }
}


void press_keys(int keys[], int length) {
  for (int k = 0; k < length; k++) {
    Keyboard.press(keys[k]);
  }
  delay(100);
  Keyboard.releaseAll();
}


bool run_json_config(String file_contents) {
  DynamicJsonDocument conf(file_contents.length() * 2);
  DeserializationError error = deserializeJson(conf, file_contents);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return false;
  }
  for (int i = 0; i < conf["steps"].size(); i++) {
    delay(100);
    if (conf["steps"][i]["action"] == "type") {
      led_on();
      type_string(conf["steps"][i]["string"]);
      led_off();
      continue;
    }
    if (conf["steps"][i]["action"] == "press") {
      int keys[conf["steps"][i]["keys"].size()];
      for (int j = 0; j < conf["steps"][i]["keys"].size(); j++) {
        keys[j] = conf["steps"][i]["keys"][j];
      }
      press_keys(keys, conf["steps"][i]["keys"].size());
      continue;
    }
    if (conf["steps"][i]["action"] == "delay") {
      delay(conf["steps"][i]["ms"]);
      continue;
    }
  }
  return true;
}


String read_file_text(String filename) {
  File file;
  // open the file for reading:
  file = SD.open(filename);
  if (file) {
    String file_contents = "";
    // read from the file until there's nothing else in it:
    while (file.available()) {
      file_contents = file_contents + String((char)file.read());
    }
    // close the file:
    file.close();
    return file_contents;
  } else {
    return "-1";
  }
}


void indicate_selected_file() {
  for (int i = 0; i < selectedFile; i++) {
    led_on();
    delay(250);
    led_off();
    delay(250);
  }
  delay(750);
}


void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print("\t");
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.print("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.print(String(entry.size()) + "\n");
    }
    entry.close();
  }
}


void setup() {
  // put your setup code here, to run once:
  pinMode(AutoRunSwitchPin, INPUT_PULLUP); // Auto Run Switch button
  pinMode(SelectButtonPin, INPUT_PULLUP); // Select button
  pinMode(TypeButtonPin, INPUT_PULLUP); // Type button
  pinMode(BlueIndicatorLEDPin, OUTPUT); // blue LED
  pinMode(SDCardCSPin, OUTPUT); // SD CS
  Serial.begin(9600);
  Keyboard.begin();
  if (!SD.begin(SDCardCSPin)) {
    Serial.print("SD Card INIT Failed!");
    return;
  }
  delay(1500);
  if (SD.exists("auto~1.jso") && digitalRead(AutoRunSwitchPin) == LOW) {
    Serial.println(read_file_text("auto~1.jso"));
    run_json_config(read_file_text("auto~1.jso"));
  }
}


void loop() {
  // put your main code here, to run repeatedly:
  val = digitalRead(SelectButtonPin);
  if (val == LOW) {
    // select button pressed
    printDirectory(SD.open("/"), 0);
    if (SD.exists(String(selectedFile + 1) + "~1.jso")) {
      selectedFile = selectedFile + 1;
    } else {
      selectedFile = 1;
    }
    indicate_selected_file();
  }
  val = digitalRead(TypeButtonPin);
  if (val == LOW) {
    // type button pressed
    indicate_selected_file();
    led_on();
    run_json_config(read_file_text(String(selectedFile) + "~1.jso"));
    led_off();
  }
}
